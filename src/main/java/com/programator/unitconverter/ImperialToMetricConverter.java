package com.programator.unitconverter;

class ImperialToMetricConverter {

    double getMetersFromFeet(double feet) {
        return feet / 3.2808;
    }

    double getCmFromInches(double in) {
        return in / 0.3937;
    }

    double getLitresFromGallons(double gal) {
        return gal / 0.26417;
    }

    double getKilogramsFromPounds(double lb) {
        return lb / 2.2046;
    }

    double getCelsiusFromFahrenheits(double fahrenheits) {
        return (fahrenheits - 32) / 1.8;
    }

}
