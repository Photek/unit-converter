package com.programator.unitconverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class UnitConverterInterface {

    private String activeConverter;
    private double valueToConvert;

    double getValueToConvert() {
        return valueToConvert;
    }

    private void setValueToConvert(double valueToConvert) {
        this.valueToConvert = valueToConvert;
    }

    String getActiveConverter() {
        return activeConverter;
    }

    private void setActiveConverter(String activeConverter) {
        this.activeConverter = activeConverter;
    }

    void showMainMenu() {
        System.out.println("*************************  UNIT CONVERTER  *************************");
        System.out.println("----------  Jednostki metryczne -> Jednostki imperialne  -----------");
        System.out.println();
        System.out.println(" 1. m -> ft (metry na stopy)");
        System.out.println(" 2. cm -> in (centymetry na cale)");
        System.out.println(" 3. l -> gal (litry na galony)");
        System.out.println(" 4. kg -> lb (kilogramy na funty)");
        System.out.println(" 5. C -> F (stopnie Celsjusza na stopnie Fahrenheita)");
        System.out.println();
        System.out.println("----------  Jednostki imperialne -> Jednostki metryczne  -----------");
        System.out.println();
        System.out.println(" 6. ft -> m (stopy na metry)");
        System.out.println(" 7. in -> cm (cale na centymetry)");
        System.out.println(" 8. gal -> l (galony na litry)");
        System.out.println(" 9. lb -> kg (funty na kilogramy)");
        System.out.println("10. F -> C (stopnie Fahrenheita na stopnie Celsjusza)");
        System.out.println();
        System.out.print("Wybierz polecenie (1-10), (0 lub exit = wyjście): ");
        mainMenuInput();
    }

    void showSlimMainMenu() {
        System.out.println();
        System.out.print("Wybierz kolejną konwersję (1-10) lub zakończ (0, exit): ");
        mainMenuInput();
    }

    private void mainMenuInput() {
        String input;
        try {
            input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    setActiveConverter("m-to-ft");
                    showUnitToConvertInputMenu("Konwersja metrów na stopy. Podaj metry (m): ");
                    break;
                case "2":
                    setActiveConverter("cm-to-in");
                    showUnitToConvertInputMenu("Konwersja centymetrów na cale. Podaj centymetry (cm): ");
                    break;
                case "3":
                    setActiveConverter("l-to-gal");
                    showUnitToConvertInputMenu("Konwersja litrów na galony. Podaj litry (l): ");
                    break;
                case "4":
                    setActiveConverter("kg-to-lb");
                    showUnitToConvertInputMenu("Konwersja kilogramów na funty. Podaj kilogramy (kg): ");
                    break;
                case "5":
                    setActiveConverter("C-to-F");
                    showUnitToConvertInputMenu("Konwersja stopni Celsjusza na stopnie Fahrenheita. Podaj stopnie Celsjusza (C): ");
                    break;
                case "6":
                    setActiveConverter("ft-to-m");
                    showUnitToConvertInputMenu("Konwersja stóp na metry. Podaj stopy (ft): ");
                    break;
                case "7":
                    setActiveConverter("in-to-cm");
                    showUnitToConvertInputMenu("Konwersja cali na centymetry. Podaj cale (in): ");
                    break;
                case "8":
                    setActiveConverter("gal-to-l");
                    showUnitToConvertInputMenu("Konwersja galonów na litry. Podaj galony (gal): ");
                    break;
                case "9":
                    setActiveConverter("lb-to-kg");
                    showUnitToConvertInputMenu("Konwersja funtów na kilogramy. Podaj funty (lb): ");
                    break;
                case "10":
                    setActiveConverter("F-to-C");
                    showUnitToConvertInputMenu("Konwersja stopni Fahrenheita na stopnie Celsjusza. Podaj stopnie Fahrenheita (F): ");
                    break;
                case "0":
                case "exit":
                    System.out.println("Bye, bye...");
                    setActiveConverter("exit");
                    break;
                default:
                    System.out.print("Błąd! Wybierz poprawne polecenie (0-10, exit): ");
                    mainMenuInput();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showUnitToConvertInputMenu(String message) {
        System.out.print(message);
        while (true) {
            String input;
            try {
                input = readInput();
                if (isInputParsableToDouble(input)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    boolean isInputParsableToDouble(String input) {
        try {
            setValueToConvert(Double.parseDouble(input.replace(',', '.')));
            return true;
        } catch (NumberFormatException e) {
            System.out.print("Błąd! Wprowadź poprawną liczbę: ");
        } catch (NullPointerException e){
            System.out.println("Błąd! Input od Użytkownika jest nullem.");
            System.out.print("Spróbuj jeszcze raz. Wprowadź poprawną liczbę: ");
        }
        return false;
    }

    private static String readInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

}
