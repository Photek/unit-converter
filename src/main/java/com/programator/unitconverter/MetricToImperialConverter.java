package com.programator.unitconverter;

class MetricToImperialConverter {

    double getFeetFromMeters(double meters) {
        return 3.2808 * meters;
    }

    double getInchesFromCm(double cm) {
        return 0.3937 * cm;
    }

    double getGallonsFromLitres(double litres) {
        return 0.26417 * litres;
    }

    double getPoundsFromKilograms(double kg) {
        return 2.2046 * kg;
    }

    double getFahrenheitsFromCelsius(double celsius) {
        return 1.8 * celsius + 32;
    }
}
