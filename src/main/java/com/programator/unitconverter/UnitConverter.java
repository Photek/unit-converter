package com.programator.unitconverter;

public class UnitConverter {
    public static void main(String[] args) {
        Converter converter = new Converter();
        UnitConverterInterface unitConverterInterface = new UnitConverterInterface();

        unitConverterInterface.showMainMenu();

        String activeConverter = unitConverterInterface.getActiveConverter();
        double valueToConvert = unitConverterInterface.getValueToConvert();

        while (!activeConverter.equals("exit")) {
            System.out.println(converter.runConversion(activeConverter, valueToConvert));
            unitConverterInterface.showSlimMainMenu();
            activeConverter = unitConverterInterface.getActiveConverter();
            valueToConvert = unitConverterInterface.getValueToConvert();
        }
    }
}
