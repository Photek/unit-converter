package com.programator.unitconverter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

class Converter {
    private MetricToImperialConverter metricToImperialConverter = new MetricToImperialConverter();
    private ImperialToMetricConverter imperialToMetricConverter = new ImperialToMetricConverter();

    String runConversion(String activeConverter, double valueToConvert) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        DecimalFormat dF = new DecimalFormat("#0.00",symbols);
        double result;
        String msg = "";

        switch (activeConverter) {
            case "m-to-ft":
                result = metricToImperialConverter.getFeetFromMeters(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " m ---to---> " + dF.format(result) + " ft";
                break;
            case "cm-to-in":
                result = metricToImperialConverter.getInchesFromCm(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " cm ---to---> " + dF.format(result) + " in";
                break;
            case "l-to-gal":
                result = metricToImperialConverter.getGallonsFromLitres(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " l ---to---> " + dF.format(result) + " gal";
                break;
            case "kg-to-lb":
                result = metricToImperialConverter.getPoundsFromKilograms(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " kg ---to---> " + dF.format(result) + " lb";
                break;
            case "C-to-F":
                result = metricToImperialConverter.getFahrenheitsFromCelsius(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " C ---to---> " + dF.format(result) + " F";
                break;

            case "ft-to-m":
                result = imperialToMetricConverter.getMetersFromFeet(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " ft ---to---> " + dF.format(result) + " m";
                break;
            case "in-to-cm":
                result = imperialToMetricConverter.getCmFromInches(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " in ---to---> " + dF.format(result) + " cm";
                break;
            case "gal-to-l":
                result = imperialToMetricConverter.getLitresFromGallons(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " gal ---to---> " + dF.format(result) + " l";
                break;
            case "lb-to-kg":
                result = imperialToMetricConverter.getKilogramsFromPounds(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " lb ---to---> " + dF.format(result) + " kg";
                break;
            case "F-to-C":
                result = imperialToMetricConverter.getCelsiusFromFahrenheits(valueToConvert);
                msg = "Wynik: " + dF.format(valueToConvert) + " F ---to---> " + dF.format(result) + " C";
                break;
        }
        return msg;
    }
}
