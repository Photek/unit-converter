package com.programator.unitconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

class MetricToImperialConversionTest {

    private MetricToImperialConverter metricToImperialConverter;
    private DecimalFormat dF;

    @BeforeEach
    void SetUp() {
        metricToImperialConverter = new MetricToImperialConverter();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        dF = new DecimalFormat("#0.00", symbols);
    }

    // Meters ---> Feet

    @Test
    void givenCorrectMetersValuePositiveInteger_ShouldReturnCorrectFeetValue() {
        double value = 1;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("3.28", dF.format(result));
    }

    @Test
    void givenCorrectMetersValuePositiveDouble_ShouldReturnCorrectFeetValue() {
        double value = 1.2345;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("4.05", dF.format(result));
    }

    @Test
    void givenCorrectMetersValueNegativeInteger_ShouldReturnCorrectFeetValue() {
        double value = -1;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("-3.28", dF.format(result));
    }

    @Test
    void givenCorrectMetersValueNegativeDouble_ShouldReturnCorrectFeetValue() {
        double value = -1.2345;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("-4.05", dF.format(result));
    }

    @Test
    void givenCorrectMetersZeroValue_ShouldReturnCorrectFeetValue() {
        double value = 0;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectMetersNegativeZeroValue_ShouldReturnCorrectFeetValue() {
        double value = -0;
        double result = metricToImperialConverter.getFeetFromMeters(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Centimeters ---> Inches

    @Test
    void givenCorrectCentimetersValuePositiveInteger_ShouldReturnCorrectInchesValue() {
        double value = 1;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("0.39", dF.format(result));
    }

    @Test
    void givenCorrectCentimetersValuePositiveDouble_ShouldReturnCorrectInchesValue() {
        double value = 1.2345;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("0.49", dF.format(result));
    }


    @Test
    void givenCorrectCentimetersValueNegativeInteger_ShouldReturnCorrectInchesValue() {
        double value = -1;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("-0.39", dF.format(result));
    }

    @Test
    void givenCorrectCentimetersValueNegativeDouble_ShouldReturnCorrectInchesValue() {
        double value = -1.2345;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("-0.49", dF.format(result));
    }

    @Test
    void givenCorrectCentimetersZeroValue_ShouldReturnCorrectInchesValue() {
        double value = 0;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectCentimetersNegativeZeroValue_ShouldReturnCorrectInchesValue() {
        double value = -0;
        double result = metricToImperialConverter.getInchesFromCm(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Litres ---> Gallons

    @Test
    void givenCorrectLitresValuePositiveInteger_ShouldReturnCorrectGallonsValue() {
        double value = 1;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("0.26", dF.format(result));
    }

    @Test
    void givenCorrectLitresValuePositiveDouble_ShouldReturnCorrectGallonsValue() {
        double value = 1.2345;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("0.33", dF.format(result));
    }

    @Test
    void givenCorrectLitresValueNegativeInteger_ShouldReturnCorrectGallonsValue() {
        double value = -1;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("-0.26", dF.format(result));
    }

    @Test
    void givenCorrectLitresValueNegativeDouble_ShouldReturnCorrectGallonsValue() {
        double value = -1.2345;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("-0.33", dF.format(result));
    }

    @Test
    void givenCorrectLitresZeroValue_ShouldReturnCorrectGallonsValue() {
        double value = 0;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectLitresNegativeZeroValue_ShouldReturnCorrectGallonsValue() {
        double value = -0;
        double result = metricToImperialConverter.getGallonsFromLitres(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Kilograms ---> Pounds

    @Test
    void givenCorrectKilogramsValuePositiveInteger_ShouldReturnCorrectPoundsValue() {
        double value = 1;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("2.20", dF.format(result));
    }

    @Test
    void givenCorrectKilogramsValuePositiveDouble_ShouldReturnCorrectPoundsValue() {
        double value = 1.2345;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("2.72", dF.format(result));
    }

    @Test
    void givenCorrectKilogramsValueNegativeInteger_ShouldReturnCorrectPoundsValue() {
        double value = -1;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("-2.20", dF.format(result));
    }

    @Test
    void givenCorrectKilogramsValueNegativeDouble_ShouldReturnCorrectPoundsValue() {
        double value = -1.2345;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("-2.72", dF.format(result));
    }

    @Test
    void givenCorrectKilogramsZeroValue_ShouldReturnCorrectPoundsValue() {
        double value = 0;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectKilogramsNegativeZeroValue_ShouldReturnCorrectPoundsValue() {
        double value = -0;
        double result = metricToImperialConverter.getPoundsFromKilograms(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Celsius ---> Fahrenheit

    @Test
    void givenCorrectCelsiusValuePositiveInteger_ShouldReturnCorrectFahrenheitsValue() {
        double value = 1;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("33.80", dF.format(result));
    }

    @Test
    void givenCorrectCelsiusValuePositiveDouble_ShouldReturnCorrectFahrenheitsValue() {
        double value = 1.2345;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("34.22", dF.format(result));
    }

    @Test
    void givenCorrectCelsiusValueNegativeInteger_ShouldReturnCorrectFahrenheitsValue() {
        double value = -1;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("30.20", dF.format(result));
    }

    @Test
    void givenCorrectCelsiusValueNegativeDouble_ShouldReturnCorrectFahrenheitsValue() {
        double value = -1.2345;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("29.78", dF.format(result));
    }

    @Test
    void givenCorrectCelsiusZeroValue_ShouldReturnCorrectFahrenheitsValue() {
        double value = 0;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("32.00", dF.format(result));
    }

    @Test
    void givenCorrectCelsiusNegativeZeroValue_ShouldReturnCorrectFahrenheitsValue() {
        double value = -0;
        double result = metricToImperialConverter.getFahrenheitsFromCelsius(value);
        Assertions.assertEquals("32.00", dF.format(result));
    }

}
