package com.programator.unitconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

class ImperialToMetricConversionTest {
    
    private ImperialToMetricConverter imperialToMetricConverter;
    private DecimalFormat dF;

    @BeforeEach
    void SetUp() {
        imperialToMetricConverter = new ImperialToMetricConverter();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        dF = new DecimalFormat("#0.00", symbols);
    }

    // Feet ---> Meters

    @Test
    void givenCorrectFeetValuePositiveInteger_ShouldReturnCorrectMetersValue() {
        double value = 1;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("0.30", dF.format(result));
    }

    @Test
    void givenCorrectFeetValuePositiveDouble_ShouldReturnCorrectMetersValue() {
        double value = 1.2345;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("0.38", dF.format(result));
    }

    @Test
    void givenCorrectFeetValueNegativeInteger_ShouldReturnCorrectMetersValue() {
        double value = -1;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("-0.30", dF.format(result));
    }

    @Test
    void givenCorrectFeetValueNegativeDouble_ShouldReturnCorrectMetersValue() {
        double value = -1.2345;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("-0.38", dF.format(result));
    }

    @Test
    void givenCorrectFeetZeroValue_ShouldReturnCorrectMetersValue() {
        double value = 0;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectFeetNegativeZeroValue_ShouldReturnCorrectMetersValue() {
        double value = -0;
        double result = imperialToMetricConverter.getMetersFromFeet(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Inches ---> Centimeters

    @Test
    void givenCorrectInchesValuePositiveInteger_ShouldReturnCorrectCentimetersValue() {
        double value = 1;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("2.54", dF.format(result));
    }

    @Test
    void givenCorrectInchesValuePositiveDouble_ShouldReturnCorrectCentimetersValue() {
        double value = 1.2345;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("3.14", dF.format(result));
    }


    @Test
    void givenCorrectInchesValueNegativeInteger_ShouldReturnCorrectCentimetersValue() {
        double value = -1;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("-2.54", dF.format(result));
    }

    @Test
    void givenCorrectInchesValueNegativeDouble_ShouldReturnCorrectCentimetersValue() {
        double value = -1.2345;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("-3.14", dF.format(result));
    }

    @Test
    void givenCorrectInchesZeroValue_ShouldReturnCorrectCentimetersValue() {
        double value = 0;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectInchesNegativeZeroValue_ShouldReturnCorrectCentimetersValue() {
        double value = -0;
        double result = imperialToMetricConverter.getCmFromInches(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Gallons ---> Litres

    @Test
    void givenCorrectGallonsValuePositiveInteger_ShouldReturnCorrectLitresValue() {
        double value = 1;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("3.79", dF.format(result));
    }

    @Test
    void givenCorrectGallonsValuePositiveDouble_ShouldReturnCorrectLitresValue() {
        double value = 1.2345;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("4.67", dF.format(result));
    }

    @Test
    void givenCorrectGallonsValueNegativeInteger_ShouldReturnCorrectLitresValue() {
        double value = -1;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("-3.79", dF.format(result));
    }

    @Test
    void givenCorrectGallonsValueNegativeDouble_ShouldReturnCorrectLitresValue() {
        double value = -1.2345;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("-4.67", dF.format(result));
    }

    @Test
    void givenCorrectGallonsZeroValue_ShouldReturnCorrectLitresValue() {
        double value = 0;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectGallonsNegativeZeroValue_ShouldReturnCorrectLitresValue() {
        double value = -0;
        double result = imperialToMetricConverter.getLitresFromGallons(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Pounds ---> Kilograms

    @Test
    void givenCorrectPoundsValuePositiveInteger_ShouldReturnCorrectKilogramsValue() {
        double value = 1;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("0.45", dF.format(result));
    }

    @Test
    void givenCorrectPoundsValuePositiveDouble_ShouldReturnCorrectKilogramsValue() {
        double value = 1.2345;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("0.56", dF.format(result));
    }

    @Test
    void givenCorrectPoundsValueNegativeInteger_ShouldReturnCorrectKilogramsValue() {
        double value = -1;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("-0.45", dF.format(result));
    }

    @Test
    void givenCorrectPoundsValueNegativeDouble_ShouldReturnCorrectKilogramsValue() {
        double value = -1.2345;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("-0.56", dF.format(result));
    }

    @Test
    void givenCorrectPoundsZeroValue_ShouldReturnCorrectKilogramsValue() {
        double value = 0;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    @Test
    void givenCorrectPoundsNegativeZeroValue_ShouldReturnCorrectKilogramsValue() {
        double value = -0;
        double result = imperialToMetricConverter.getKilogramsFromPounds(value);
        Assertions.assertEquals("0.00", dF.format(result));
    }

    // Fahrenheit ---> Celsius

    @Test
    void givenCorrectFahrenheitValuePositiveInteger_ShouldReturnCorrectCelsiusValue() {
        double value = 1;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-17.22", dF.format(result));
    }

    @Test
    void givenCorrectFahrenheitValuePositiveDouble_ShouldReturnCorrectCelsiusValue() {
        double value = 1.2345;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-17.09", dF.format(result));
    }

    @Test
    void givenCorrectFahrenheitValueNegativeInteger_ShouldReturnCorrectCelsiusValue() {
        double value = -1;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-18.33", dF.format(result));
    }

    @Test
    void givenCorrectFahrenheitValueNegativeDouble_ShouldReturnCorrectCelsiusValue() {
        double value = -1.2345;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-18.46", dF.format(result));
    }

    @Test
    void givenCorrectFahrenheitZeroValue_ShouldReturnCorrectCelsiusValue() {
        double value = 0;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-17.78", dF.format(result));
    }

    @Test
    void givenCorrectFahrenheitNegativeZeroValue_ShouldReturnCorrectCelsiusValue() {
        double value = -0;
        double result = imperialToMetricConverter.getCelsiusFromFahrenheits(value);
        Assertions.assertEquals("-17.78", dF.format(result));
    }

}
