package com.programator.unitconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConverterTest {

    private Converter converter;

    @BeforeEach
    void SetUp(){
        converter = new Converter();
    }

    @Test
    void givenCorrectMetersValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "m-to-ft";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 m ---to---> 32,81 ft", result);
    }

    @Test
    void givenCorrectCentimetersValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "cm-to-in";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 cm ---to---> 3,94 in", result);
    }

    @Test
    void givenCorrectLitresValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "l-to-gal";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 l ---to---> 2,64 gal", result);
    }

    @Test
    void givenCorrectKilogramsValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "kg-to-lb";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 kg ---to---> 22,05 lb", result);
    }

    @Test
    void givenCorrectCelsiusValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "C-to-F";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 C ---to---> 50,00 F", result);
    }

    // Imperial

    @Test
    void givenCorrectFeetValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "ft-to-m";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 ft ---to---> 3,05 m", result);
    }

    @Test
    void givenCorrectInchesValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "in-to-cm";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 in ---to---> 25,40 cm", result);
    }

    @Test
    void givenCorrectGallonsValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "gal-to-l";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 gal ---to---> 37,85 l", result);
    }

    @Test
    void givenCorrectPoundsValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "lb-to-kg";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 lb ---to---> 4,54 kg", result);
    }

    @Test
    void givenCorrectFahrenheitValue_ShouldReturnCorrectResultMessage() {
        String activeConverter = "F-to-C";
        double value = 10;
        String result = converter.runConversion(activeConverter,value);
        Assertions.assertEquals("Wynik: 10,00 F ---to---> -12,22 C", result);
    }



}
