package com.programator.unitconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UnitConverterInterfaceTest {

    private UnitConverterInterface unitConverterInterface;

    @BeforeEach
    void SetUp(){
        unitConverterInterface = new UnitConverterInterface();
    }

    @Test
    void givenCorrectIntegerValueFromUserInput_ShouldReturnTrue(){
        String value = "3";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenCorrectNegativeIntegerValueFromUserInput_ShouldReturnTrue(){
        String value = "-3";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenCorrectDoubleValueWithCommaSeparatorFromUserInput_ShouldReturnTrue(){
        String value = "3,5";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenCorrectDoubleValueWithDotSeparatorFromUserInput_ShouldReturnTrue(){
        String value = "3.5";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenCorrectDoubleNegativeValueWithCommaSeparatorFromUserInput_ShouldReturnTrue(){
        String value = "-3,5";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenCorrectDoubleNegativeValueWithDotSeparatorFromUserInput_ShouldReturnTrue(){
        String value = "-3.5";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertTrue(result);
    }

    @Test
    void givenNotValidNumber_ShouldReturnFalse(){
        String value = "not a number";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertFalse(result);
    }

    @Test
    void givenValidNumberWithWrongSeparator_ShouldReturnFalse(){
        String value = "3'5";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertFalse(result);
    }

    @Test
    void givenEmptyInput_ShouldReturnFalse(){
        String value = "";
        boolean result = unitConverterInterface.isInputParsableToDouble(value);
        Assertions.assertFalse(result);
    }

    @Test
    void givenNullInput_ShouldReturnFalse(){
        boolean result = unitConverterInterface.isInputParsableToDouble(null);
        Assertions.assertFalse(result);
    }

}
